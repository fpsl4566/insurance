package com.iy.insuranceconsultationapi.repository;


import com.iy.insuranceconsultationapi.entity.Insurance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InsuranceRepository extends JpaRepository<Insurance, Long> {
}
