package com.iy.insuranceconsultationapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor

public enum Gender {
    WOMEN("여자"),
    MAN("남자");

    private final String genders;
}
