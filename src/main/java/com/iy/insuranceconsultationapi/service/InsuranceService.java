package com.iy.insuranceconsultationapi.service;

import com.iy.insuranceconsultationapi.entity.Insurance;
import com.iy.insuranceconsultationapi.model.InsuranceRequest;
import com.iy.insuranceconsultationapi.repository.InsuranceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor

public class InsuranceService {
    private final InsuranceRepository insuranceRepository;

    public void setInsurance(InsuranceRequest request){
        Insurance addData = new Insurance();
        addData.setDateMaker(LocalDate.now());
        addData.setName(request.getName());
        addData.setBirthDay(request.getBirthDay());
        addData.setMobile(request.getMobile());
        addData.setGender(request.getGender());

        insuranceRepository.save(addData);
    }


}
