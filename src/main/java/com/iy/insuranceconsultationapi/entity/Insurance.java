package com.iy.insuranceconsultationapi.entity;

import jakarta.persistence.*;
import jakarta.websocket.ClientEndpoint;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter

public class Insurance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate dateMaker;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false,length = 15)
    private Integer birthDay;

    @Column(nullable = false, length = 20)
    private  String mobile;

    @Column(nullable = false)
    private  String gender;


}
