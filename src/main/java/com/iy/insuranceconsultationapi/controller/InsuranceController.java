package com.iy.insuranceconsultationapi.controller;

import com.iy.insuranceconsultationapi.model.InsuranceRequest;
import com.iy.insuranceconsultationapi.service.InsuranceService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("Insurance")
public class InsuranceController {
    private final InsuranceService insuranceService;

    @PostMapping("new")
    public String InsuranceService(@RequestBody InsuranceRequest request){
        insuranceService.setInsurance(request);

        return "OK";

}

}
