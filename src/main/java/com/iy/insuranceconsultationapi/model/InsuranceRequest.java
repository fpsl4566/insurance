package com.iy.insuranceconsultationapi.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class InsuranceRequest {
        private String name;
        private Integer birthDay;
        private String mobile;
        private String gender;

}
